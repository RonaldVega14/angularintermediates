import { Component } from '@angular/core';
import { Logger } from './logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'AngularIntermediate';
  username: string;
  password: string;
  loggedIn = false;

  constructor(private logger: Logger) {
  }

  login() {
    this.logger.login(this.username, this.password).subscribe(loggedIn => {
      if (loggedIn === false) {
        alert('Wrong Password');
      }
      this.loggedIn = loggedIn;
    });
  }
}
