import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';

@Injectable({
  providedIn: 'root'
})
export class Logger {

  login(username: string, pass: string): Observable<boolean> {
    let success = false;
    console.log('username:', username);
    console.log('pass:', pass);
    users.forEach(user => {
      if (username === user.username && pass === user.password) {
        success = true;
      }
    });
    return of(success);
  }
}

export const users = [
  { password: '123', username: 'bill' },
  { password: '1234', username: 'bob' },
];
