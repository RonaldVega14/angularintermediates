import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    //You need to add services here to use them, or add the metadata 'ProvidedIn' 
    // to provide the service from the @injector
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
